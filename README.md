# Chris Brooks Website | 2017

# Getting Started

To start project run the following commands:-

```
npm install
npm start
```
# Front-End Stack

- HTML5/CSS3
- Gulp
- NPM
- Bootstrap
- Font Awesome
- Hover.css
- Bitbucket Pipelines (continuous integration)

# Push to live

1. Add a "dev" tag: git tag dev-1.4-new-colors
2. Push with tags: git push origin master --tags

# Push to dev

1. Add a "dev" tag: git tag dev-1.4-new-colors
2. Push with tags: git push origin master --tags

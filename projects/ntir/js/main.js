(function($) {
    $(document).ready(function() {
       
        $(document.body).on('click', '.component-vertical-floating-bar .tray-toggle', function(event) {
            $(this).closest('.component-vertical-floating-bar').find('.tray').toggleClass('show');
            $(this).closest('.component-vertical-floating-bar').toggleClass('is-open');
        });

        // I'm not sure what the following code do.
        var selector = '.mdl-navigation a';

        $(selector).on('click', function() {
            $(selector).removeClass('is-active');
            $(this).addClass('is-active');
        });

        // Following code makes filter options toggleable.
        $(document.body).on('click', '#search-bar__search-options', function(event){
            $(this).find('.icon-show').toggleClass('hide');
            $(this).find('.icon-hide').toggleClass('hide');
            $('.component-filter-bar').toggleClass('hide');
        });
 
    }); 
})(jQuery);

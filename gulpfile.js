/**
 * Plugins
 * =======
 */

// Gulp, the task runner.
var gulp = require('gulp');

// Gulp plugin to remove or replace relative path for files.
// var flatten = require('gulp-flatten');

// Gulp plugin for SASS preprocessor.
var sass = require('gulp-sass');

// Gulp plugin to minify CSS, using clean-css.
var cleanCSS = require('gulp-clean-css');

// Gulp plugin to concatenates files.
var concat = require('gulp-concat');

// Gulp plugin to minify files with UglifyJS (javascript files minification).
var uglify = require('gulp-uglify');

// Gulp plugin for desktop notifications.
var notify = require("gulp-notify");

// Gulp plugin for renaming files.
// var rename = require("gulp-rename");

// Gulp plugin for source maps generation.
var sourcemaps = require('gulp-sourcemaps');

// Gulp plugin to filter files from the stream.
// var filter = require('gulp-filter');

// BrowserSync, to auto-refresh browser when said files are modified.
var browserSync = require('browser-sync').create();

/**
 * Configurations
 * ==============
 */

// Globs of font files that we want to pack to 'dist' folder.
// var fontGlobs = [
//     'node_modules/bootstrap/dist/fonts/*.{otf,eot,svg,ttf,woff,woff2,eof}',
//     'node_modules/font-awesome/fonts/*.{otf,eot,svg,ttf,woff,woff2,eof}',
//     'src/fonts/*.{otf,eot,svg,ttf,woff,woff2,eof}',
//     'src/fonts/**/*.{otf,eot,svg,ttf,woff,woff2,eof}',
// ];

// Globs of CSS and SCSS files that we want to convert to CSS and then compile
// into a single file.
var scssFilesToConvert = [
    'src/scss/main.scss'
];

var scssFilesToWatch = [
    'src/scss/*.scss'
];

var jsFilesToCombine = [
    'src/js/main.js'
];

var jsFilesToWatch = [
    'src/js/*.js'
];

gulp.task('serve', function() {
    browserSync.init({
        server: "./",
    });
    gulp.watch([scssFilesToWatch], ['scss']);
    gulp.watch([jsFilesToWatch], ['js']);
    gulp.watch("./index.html").on('change', browserSync.reload);
});

gulp.task('scss', function() {
    return gulp.src(scssFilesToConvert)
        .pipe(sass({ errLogToConsole: true }))
        .pipe(concat('main.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./dist/css/'))
        .pipe(notify({
            title: 'SCSS Completed'
        }))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    return gulp.src(jsFilesToCombine)
        .pipe(concat('main.min.js'))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/js/'))
        .pipe(notify({
            title: 'JS Completed'
        }))
        .pipe(browserSync.stream());
});

// gulp.task('move-fonts', function(){
//     return gulp.src(fontGlobs)
//         .pipe(flatten())
//         .pipe(gulp.dest('./dist/fonts/'));
// });

// gulp.task('move-images', function() {
//     return gulp.src("./src/img/")
//         .pipe(gulp.dest('./dist/img/'));
// });

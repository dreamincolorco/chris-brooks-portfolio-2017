// Fixed navbar display/hide functionality
(function($){
    $(document).ready(function(){
        $(window).scroll(function() {
          var scrollTop = $(window).scrollTop();
          if ( scrollTop > $('.section-about-us').offset().top - 85 ) {
         /*   $('.navbar').removeClass('hide');*/
          } else {
/*            $('.navbar').addClass('hide');*/
          }
        });
    });
})(jQuery);

// Form submission.
(function($){
  $(document).ready(function(){
    $(document).on('submit', '#contact-form', function(e){
      e.preventDefault();
      var datastring = $("#contact-form").serialize();
      $.ajax({
          type: "POST",
          url: "/submit.php",
          data: datastring,
          beforeSend: function() {
               $('.section-contact-us .form-status').addClass('hide');
               $('.section-contact-us .form-status-info').removeClass('hide');
          },
          success: function(data) {
            console.log(data);
            console.log(data == 1);
             if(data == 1) {
               $('.section-contact-us .form-status').addClass('hide');
               $('.section-contact-us .form-status-success').removeClass('hide');
             } else {
               $('.section-contact-us .form-status').addClass('hide');
               $('.section-contact-us .form-status-fail').removeClass('hide');
             }
          },
          error: function() {
              $('.section-contact-us .form-status').addClass('hide');
              $('.section-contact-us .form-status-fail').removeClass('hide');
          }
      });
    });
  });
})(jQuery);

// Menu overlay functionality
(function($){
  $(document).ready(function(){
    $(document).on('click', '.section-hero .menu-button', function(e){
      e.preventDefault();
      $('.section-menu-overlay').addClass('show');
      $('html').addClass('disable-scroll');
      $('body').addClass('disable-scroll');
    });
    $(document).on('click', '.section-menu-overlay .menu-button-close', function(e){
      e.preventDefault();
      $('.section-menu-overlay').removeClass('show');
      $('html').removeClass('disable-scroll');
      $('body').removeClass('disable-scroll');
    });
  });
})(jQuery);

// Fixed navbar changing active.
(function($){
    $(document).ready(function(){
        $(window).scroll(function() {
          var scrollTop = $(window).scrollTop();
          var navbar_thresh_hold = 80;
          var sections = [
            'section-hero',
            'section-about-us',
            'section-clients-and-partners',
            'section-portfolio-wrapper',
            'section-our-services',
            'section-contact-us'
          ]
          for (var i in sections) {
            var elem = $('.' + sections[i]);
            var elem_top = elem.offset().top - navbar_thresh_hold;
            var elem_bottom = elem.offset().top + elem.outerHeight(true) - navbar_thresh_hold;
            if ( scrollTop >= elem_top && scrollTop < elem_bottom ) {
              $('.nav-item[data-section-name="' + sections[i] + '"]').addClass('active');
            } else {
              $('.nav-item[data-section-name="' + sections[i] + '"]').removeClass('active');
            }
          }
        });
    });
})(jQuery);

// Fixed navbar scroll to element functionality.
(function($){
    $(document).ready(function() {
      $(document).on('click', '.navbar .nav-item', function(e){
        e.preventDefault();
        var section = $(this).attr('data-section-name');
        $('html, body')
          .animate({
              scrollTop: $('.' + section).offset().top - 80
          },'slow');
      });
    });
})(jQuery);

// Section menu overlay scroll to element functionality.
(function($){
    $(document).ready(function() {
      $(document).on('click', '.section-menu-overlay .nav-item', function(e){
        e.preventDefault();
        $('.section-menu-overlay').removeClass('show');
        $('html').removeClass('disable-scroll');
        $('body').removeClass('disable-scroll');
        var section = $(this).attr('data-section-name');
        $('html, body')
          .animate({
              scrollTop: $('.' + section).offset().top - 80
          },'slow');
      });
    });
})(jQuery);

// Rotating Words
/*(function($){
    var revolving_one = $(".rotating-words-1 span");
    var revolving_one_options = -1;
    function showNextQuote() {
     $(".rotating-words-1 span").removeClass("animate");
        ++revolving_one_options;
        revolving_one.eq(revolving_one_options % revolving_one.length)
            .fadeIn(1000)
            .delay(500)
                       .addClass("animate")
            .fadeOut(1000, showNextQuote);
    }
       showNextQuote();

    function showNextQuote_two() {
          var revolving_two = $(".rotating-words-2 span");
    var revolving_two_options = -1;
     $(".rotating-words-2 span").removeClass("animate");
        ++revolving_two_options;
        revolving_two.eq(revolving_two_options % revolving_two.length)
            .fadeIn(1000)
            .delay(500)
            .addClass("animate")
            .fadeOut(1000, showNextQuote_two);
    }
    showNextQuote_two();
})(jQuery);*/
